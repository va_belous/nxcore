FROM nginx:1.23.0
ARG DIST_PATH
ADD $DIST_PATH /var/www
RUN cd /var/www && ls -la
