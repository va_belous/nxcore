import { Component } from '@angular/core';

@Component({
  selector: 'nx-core-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'nx-core';
}
