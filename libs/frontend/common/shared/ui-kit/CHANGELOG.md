## [1.0.5](https://gitlab.com/va_belous/nxcore/compare/ui-kit-v1.0.4...ui-kit-v1.0.5) (2023-10-12)


### Bug Fixes

* Update component ([c55360e](https://gitlab.com/va_belous/nxcore/commit/c55360e6cbca4f13486955b4176b192650e6749c))

## [1.0.4](https://gitlab.com/va_belous/nxcore/compare/ui-kit-v1.0.3...ui-kit-v1.0.4) (2023-10-10)


### Bug Fixes

* Update ([224803e](https://gitlab.com/va_belous/nxcore/commit/224803ebe8495fcb7e9ddba6d87ec75e37fcd1ac))
* Update ([62a45e6](https://gitlab.com/va_belous/nxcore/commit/62a45e6af4c0d79dc5c1af0dcdbccc7764652059))

## [1.0.3](https://gitlab.com/va_belous/nxcore/compare/ui-kit-v1.0.2...ui-kit-v1.0.3) (2023-10-09)


### Bug Fixes

- Update ([e3443f6](https://gitlab.com/va_belous/nxcore/commit/e3443f6200935c399bce4f2a95b3881635a5a5aa))
- Update ([ba06a14](https://gitlab.com/va_belous/nxcore/commit/ba06a14c1001ed7873e5af9ca62db64f3fc17cdc))

## [1.0.2](https://gitlab.com/va_belous/nxcore/compare/ui-kit-v1.0.1...ui-kit-v1.0.2) (2023-10-09)

### Bug Fixes

- Update ([2eb4f30](https://gitlab.com/va_belous/nxcore/commit/2eb4f30b0297fdc024cb03a8c82fc6ef78572fe4))
- Update ([f117f1e](https://gitlab.com/va_belous/nxcore/commit/f117f1e6d4ca650edf4be7373468b26481a8b77b))

## [1.0.1](https://gitlab.com/va_belous/nxcore/compare/ui-kit-v1.0.0...ui-kit-v1.0.1) (2023-10-09)

### Bug Fixes

- Update ([af4b20f](https://gitlab.com/va_belous/nxcore/commit/af4b20ffc266d739d4a662be49068367582be020))
- Update ([36bb288](https://gitlab.com/va_belous/nxcore/commit/36bb28837b61d9be117d9247a02fdb3db7efc451))
- Update ([5c22829](https://gitlab.com/va_belous/nxcore/commit/5c228296f7c250753762e1525585fba66ec9a9a8))

# 1.0.0 (2023-10-09)

### Bug Fixes

- Add component ([b2fc076](https://gitlab.com/va_belous/nxcore/commit/b2fc076e6bdf1dde5a4982ed7b0bf8d59311fe7d))
- Update ([0e911bb](https://gitlab.com/va_belous/nxcore/commit/0e911bbeeec628433e16b841983cef227eeb6043))
- Update ([52d7b38](https://gitlab.com/va_belous/nxcore/commit/52d7b386192a3efa95d593ea501d159efd638344))
- Update ([a56fa9c](https://gitlab.com/va_belous/nxcore/commit/a56fa9cc15b224f25097afc9d3f86dcba17db2bc))
- Update ([1f00b94](https://gitlab.com/va_belous/nxcore/commit/1f00b9444a72b33680b5158dd699b69752271081))
- Update ([8069fe8](https://gitlab.com/va_belous/nxcore/commit/8069fe8473d7f98bb8184e4fdc190fee611d494e))
- Update ([bd0f515](https://gitlab.com/va_belous/nxcore/commit/bd0f515d7cb9fab9ea4c86a0bc5c619f707c3a69))
- Update ([aad3d8f](https://gitlab.com/va_belous/nxcore/commit/aad3d8f94dd6297a958cc9a750bcdc4ccd76d43b))
- Update ([8ea03bd](https://gitlab.com/va_belous/nxcore/commit/8ea03bdad6821b45453f29cd360c86453adc55ab))
- Update ([faee47f](https://gitlab.com/va_belous/nxcore/commit/faee47f7f1fce183fe80f022eb719482efb30e32))
- Update ([743885b](https://gitlab.com/va_belous/nxcore/commit/743885b09dbf020fe2062ad40226bca494ca727c))
- Update ([c118904](https://gitlab.com/va_belous/nxcore/commit/c1189044e934545918e086709037ed7483a9025d))
- Update ([5f73095](https://gitlab.com/va_belous/nxcore/commit/5f73095a66480a87ec51a4e8a94d002579a4fb39))
- Update ([97e7b9d](https://gitlab.com/va_belous/nxcore/commit/97e7b9df2e2da83962f0dd1f0c9fd60724ae0f51))
- Update ([aa48dc0](https://gitlab.com/va_belous/nxcore/commit/aa48dc09572ea8495e1c9e4d7a31211acf8b39d8))
- UPDATE ([ed3c74f](https://gitlab.com/va_belous/nxcore/commit/ed3c74fe763edec14e7acfb251415cf5c2a270a9))
- Update 1 ([a2ddb8d](https://gitlab.com/va_belous/nxcore/commit/a2ddb8d3a064616fcf084c5cd63ebecbf1c2f2f7))
- Update ciomponent ([7fad353](https://gitlab.com/va_belous/nxcore/commit/7fad353b4816d82381c65b4761d9fa262eec9ebe))
- Update lib ([ada6d6f](https://gitlab.com/va_belous/nxcore/commit/ada6d6f3f28f39d7d6f2c763a46f9caaec9acbed))

### Features

- Add lib ([a75236c](https://gitlab.com/va_belous/nxcore/commit/a75236ce2229c349717c84c7d8552e5670449a06))
- Add project ([2a9a60f](https://gitlab.com/va_belous/nxcore/commit/2a9a60f7d5b84906efb8c2644b3f0df730cf877f))
- Add ui lib ([b00e483](https://gitlab.com/va_belous/nxcore/commit/b00e4839866ebb5344ce7e9c8826649ce1debf82))
- Update lib ([079cae0](https://gitlab.com/va_belous/nxcore/commit/079cae0058dc42b3547d543f6ffd98d203eb3f9c))
