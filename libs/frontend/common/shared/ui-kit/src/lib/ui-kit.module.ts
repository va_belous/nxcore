import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErComponent } from './er/er.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ErComponent],
})
export class UiKitModule {}
