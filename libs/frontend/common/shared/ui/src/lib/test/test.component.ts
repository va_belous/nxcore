import { Component } from '@angular/core';

@Component({
  selector: 'nx-core-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
})
export class TestComponent {}
